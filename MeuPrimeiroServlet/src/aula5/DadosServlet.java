package aula5;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Dados Servlet", urlPatterns = "/dados")
public class DadosServlet extends HttpServlet {

	private static final long serialVersionUID = 4328269177880408186L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*
		 * resp.setContentType("text/html");
		 * 
		 * PrintWriter out = resp.getWriter();
		 * out.println("<h1>Cadastro enviado com sucesso</h1>");
		 * 
		 * String nome = req.getParameter("nome"); String email =
		 * req.getParameter("email"); String telefone = req.getParameter("telefone");
		 * String data = req.getParameter("data");
		 * 
		 * out.println("<ul>"); out.println("<li>Nome: " + nome + "</li>");
		 * out.println("<li>Email: " + email + "</li>"); out.println("<li>Telefone: " +
		 * telefone + "</li>"); out.println("<li>Data: " + data + "</li>");
		 * out.println("</ul>");
		 */
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");

		PrintWriter out = resp.getWriter();
		out.println("<h1>Cadastro enviado com sucesso</h1>");

		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String telefone = req.getParameter("telefone");
		String data = req.getParameter("data");

		try (FileWriter file = new FileWriter("C:\\Users\\Stiven O Bezerra\\dados.txt");
				PrintWriter write = new PrintWriter(file)) {
			write.println("<ul>");
			write.println("<li>Nome: " + nome + "</li>");
			write.println("<li>Email: " + email + "</li>");
			write.println("<li>Telefone: " + telefone + "</li>");
			write.println("<li>Data: " + data + "</li>");
			write.println("</ul>");
		}
	}
}
