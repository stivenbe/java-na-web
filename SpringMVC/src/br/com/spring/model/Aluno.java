package br.com.spring.model;

public class Aluno {
	private String nome;
	private String dataNasc;

	public Aluno() {
		super();
	}

	public Aluno(String nome, String dataNasc) {
		super();
		this.nome = nome;
		this.dataNasc = dataNasc;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}

}
