package br.com.struts.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

import br.com.struts.modelo.Pessoa;

@Namespace("/pessoa")
public class PessoaAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private Pessoa pessoa;
	private String mensagem;
	private List<String> pessoas;
		
	@Action(value = "/", results = { @Result(name = "success", location = "/Cadastro.jsp"),
			@Result(name = "error", location = "/erro.jsp") })
	public String abrirFormulario() throws Exception {
		pessoa = new Pessoa();
		return SUCCESS;
	}
	
	@Action(value = "/cadastrar", results = {
			@Result(name = "success", location = "/sucesso.jsp"),
			@Result(name = "error", location = "erro.jsp"),
	})
	public String cadastrar() {
		System.out.println(this.pessoa);
		this.mensagem = "Cadastro com sucesso!";
		return SUCCESS;
	}
	
	public String execute() {

		pessoas = new ArrayList<String>();
		pessoas.add("Snack Plate");
		pessoas.add("Dinner Plate");
		pessoas.add("Colonel Chicken Rice Combo");
		pessoas.add("Colonel Burger");
		pessoas.add("O.R. Fillet Burger");
		pessoas.add("Zinger Burger");

		return SUCCESS;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<String> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<String> pessoas) {
		this.pessoas = pessoas;
	}
}
